#include "point.hpp"
#include "cartesien.hpp"
#include "polaire.hpp"

Point::Point() {}

void Point::afficher(std::stringstream & flux) const {}

std::stringstream& operator<<(std::stringstream& ss, const Point& p) {
    p.afficher(ss);
    return ss;
}