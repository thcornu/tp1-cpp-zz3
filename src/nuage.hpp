#pragma once

#include <vector>

class Point;
class Cartesien;

class Nuage {
    std::vector<Point*> tab;

    public :
        void ajouter(Point& p);
        unsigned int size() const;

        using const_iterator = std::vector<Point*>::const_iterator;
        const_iterator begin() const;
        const_iterator end() const;

};

Cartesien barycentre(Nuage& n);