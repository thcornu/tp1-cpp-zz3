#pragma once

#include <sstream>

class Cartesien;
class Polaire;

class Point {
    public :
        Point();
        virtual void afficher(std::stringstream & flux) const ;
        virtual void convertir(Cartesien& c) const = 0;
        virtual void convertir(Polaire& p) const = 0;
        friend std::stringstream& operator<<(std::stringstream& ss, 
            const Point& p);
};