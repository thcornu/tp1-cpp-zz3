#include "polaire.hpp"
#include "cartesien.hpp"
#include <math.h>

Polaire::Polaire(double a, double d) : a(a), d(d) {}
Polaire::Polaire(Cartesien& c) : a(atan(c.getY()/c.getX())*(180/M_PI)), 
    d(sqrt(c.getX()*c.getX()+c.getY()*c.getY())) {}

double Polaire::getAngle() const {return a;}
void Polaire::setAngle(double _a) {a = _a;}
double Polaire::getDistance() const {return d;}
void Polaire::setDistance(double _d) {d = _d;}
void Polaire::afficher(std::stringstream & flux) const {
    flux << "(a=" << a << ";d=" << d << ")";
}
void Polaire::convertir(Cartesien& c) const {
    c.setX(d*cos(a*(M_PI/180)));
    c.setY(d*sin(a*(M_PI/180)));
}
void Polaire::convertir(Polaire& p) const {
    p.setDistance(d);
    p.setAngle(a);
}

std::stringstream& operator<<(std::stringstream& ss, const Polaire& p) {
    p.afficher(ss);
    return ss;
}