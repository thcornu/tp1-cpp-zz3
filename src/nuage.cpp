#include "nuage.hpp"
#include "point.hpp"
#include "cartesien.hpp"

void Nuage::ajouter(Point& p) {
    tab.push_back(&p);
}
unsigned int Nuage::size() const {
    return tab.size();
}

Cartesien barycentre(Nuage& n) {
    unsigned int vectorSize = n.size();
    double _x = 0;
    double _y = 0;

    
    for (Nuage::const_iterator it = n.begin(); it != n.end(); ++it) {
        Cartesien & c = *static_cast<Cartesien *>(*it);
        _x += c.getX();
        _y += c.getY();
    }
    _x /= vectorSize;
    _y /= vectorSize;

    return {_x, _y};
}


Nuage::const_iterator Nuage::begin() const {
    return tab.begin();
}

Nuage::const_iterator Nuage::end() const {
    return tab.end();
}